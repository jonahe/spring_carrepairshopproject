package se.erikwiberg.carrepairshop.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import se.erikwiberg.carrepairshop.models.CarModel;
import se.erikwiberg.carrepairshop.services.CarService;

@RestController
@RequestMapping("/api")
public class CarController {

	@Autowired
	private CarService carService;
	
	@CrossOrigin
	@RequestMapping(value = "/cars/", method = RequestMethod.GET)
	public ResponseEntity<List<CarModel>> getAllCars(
			@RequestParam(name="searchTerm", required=false) String searchTerm) {
	
		List<CarModel> cars;
		
		if(searchTermIsNullOrEmpty(searchTerm)) {
			cars = carService.getAll();
		} else {
			cars = carService.getBySearchString(searchTerm);
		}
		
		return new ResponseEntity<List<CarModel>>(cars, HttpStatus.OK);
	}
	
	@CrossOrigin
	@RequestMapping(value = "/cars/", method = RequestMethod.POST)
	public ResponseEntity<CarModel> createCar(@RequestBody CarModel car) {
		CarModel savedCar = carService.save(car);	
		return new ResponseEntity<CarModel>(savedCar, HttpStatus.OK);
	}
	
	@CrossOrigin
	@RequestMapping(value = "/cars/{id}/", method = RequestMethod.GET)
	public ResponseEntity<CarModel> getCarById(@PathVariable Long id) {
		CarModel car = carService.getById(id);
		return new ResponseEntity<CarModel>(car, HttpStatus.OK);
	}
	
	@CrossOrigin
	@RequestMapping(value= "/cars/{id}/", method = RequestMethod.PUT)
	public ResponseEntity<CarModel> updateCar(@PathVariable Long id, @RequestBody CarModel car) {
		CarModel updatedCar = carService.save(car);
		return new ResponseEntity<CarModel>(updatedCar, HttpStatus.OK);
	}
	
	@CrossOrigin
	@RequestMapping(value = "/cars/{id}/", method = RequestMethod.DELETE)
	public void deleteCarById(@PathVariable Long id) {
		carService.deleteById(id);
	}
	
	private boolean searchTermIsNullOrEmpty(String searchTerm){
		return searchTerm == null || searchTerm.isEmpty();
	}
	
}
