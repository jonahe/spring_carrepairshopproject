package se.erikwiberg.carrepairshop.controllers;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import se.erikwiberg.carrepairshop.models.StatisticsModel;

/**
 * 
 * Count how many 'entities' fall into a 'category'.
 * For example: how many cars fall into the 'category' that their owner has user type of CUSTOMER
 * The allowed values to group by (valid column names in each table/view) 
 * are hard-coded in the front-end side. See: /resources/static/js/controller/statistics-controller.js
 * 
 * Note: MySQL views are created in /resources/data.sql, loaded by Spring Boot
 *
 */
@RestController
@RequestMapping("/api")
public class StatisticsController {
	
	@PersistenceContext
	@Autowired
	private EntityManager entityManager;
	
	
	@CrossOrigin
	@RequestMapping(value = "/statistics/cars/", method = RequestMethod.GET)
	public ResponseEntity<StatisticsModel> getCarStatistics(@RequestParam(name="columnName", required=true) String columnName){
		
		StatisticsModel statisticsModel = 
				getStatisticsModelFromTableAndColumnName("view_car_info", columnName);
		
		return new ResponseEntity<StatisticsModel>(statisticsModel, HttpStatus.OK);
	}
	
	@CrossOrigin
	@RequestMapping(value = "/statistics/repairappointments/", method = RequestMethod.GET)
	public ResponseEntity<StatisticsModel> getRepairAppointmentStatistics(@RequestParam(name="columnName", required=true) String columnName){

		StatisticsModel statisticsModel = 
				getStatisticsModelFromTableAndColumnName("view_appointments_info", columnName);
		
		return new ResponseEntity<StatisticsModel>(statisticsModel, HttpStatus.OK);
	}
	
	@CrossOrigin
	@RequestMapping(value = "/statistics/users/", method = RequestMethod.GET)
	public ResponseEntity<StatisticsModel> getUsersStatistics(@RequestParam(name="columnName", required=true) String columnName){

		StatisticsModel statisticsModel = 
				getStatisticsModelFromTableAndColumnName("view_user_info", columnName);
		
		return new ResponseEntity<StatisticsModel>(statisticsModel, HttpStatus.OK);
	}
	
	
	private <T> StatisticsModel getStatisticsModelFromTableAndColumnName(String tableOrViewName, String columnName) {
		
		String queryString = buildSQLQueryForTableAndGroupBy(tableOrViewName, columnName);
		Query query = entityManager.createNativeQuery(queryString);
		
		
		List<Object[]> resultRows = query.getResultList();
		// why List<Object> ? see http://stackoverflow.com/a/11502765
		
		StatisticsModel statisticsModel = createStatisticsModelFromResult(resultRows);
		return statisticsModel;
	}
	
	
	private StatisticsModel createStatisticsModelFromResult (List<Object[]> resultRowList){
		
		StatisticsModel statisticsModel = new StatisticsModel();
		
		final int ENTITY_NAME_INDEX = 0;
		final int ENTITY_COUNT_INDEX = 1;
		
		for(Object[] resultRow : resultRowList) {
			
			String entityName = (String) resultRow[ENTITY_NAME_INDEX];
			BigInteger entityCount = (BigInteger) resultRow[ENTITY_COUNT_INDEX];
			
			statisticsModel.putEntityNameEntityCountPair(entityName, entityCount.intValue());
		}
		
		return statisticsModel;
	}
	
	
	private <T> String buildSQLQueryForTableAndGroupBy(String tableOrViewName, String columnToGroupBy) {
		
		String sqlQueryWithoutParameters = 
				"SELECT "
				+ "`%1$s`, "
				+ "count(*) as 'entity count' "
				+ "FROM %2$s "
				+ "GROUP BY `%1$s` ";
		
		// for year/month, the relevant sorting is by date. for all others, sort by count
		String orderBy = columnToGroupBy.equals("year/month") ? "ORDER BY date DESC" : "ORDER BY `entity count` DESC";
		sqlQueryWithoutParameters += orderBy;
		
		String fullSQLQuery = String.format(sqlQueryWithoutParameters, columnToGroupBy, tableOrViewName);
		System.out.println("Full statistics query is: " + fullSQLQuery);
		
		return fullSQLQuery;
	}
	
	
}
