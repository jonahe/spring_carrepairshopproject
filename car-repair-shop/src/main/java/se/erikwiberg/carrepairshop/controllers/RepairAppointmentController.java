package se.erikwiberg.carrepairshop.controllers;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import se.erikwiberg.carrepairshop.models.RepairAppointmentModel;
import se.erikwiberg.carrepairshop.services.RepairAppointmentService;

@RestController
@RequestMapping("/api")
public class RepairAppointmentController {
	
	@Autowired
	private RepairAppointmentService repairAppointmentService;
	
	@CrossOrigin
	@RequestMapping(value = "/repairappointments/", method = RequestMethod.GET)
	public ResponseEntity<List<RepairAppointmentModel>> getAllRepairAppointments(
			@RequestParam(name="searchTerm", required=false) String searchTerm
			) {
		
		List<RepairAppointmentModel> appointments;
		
		if(searchTermIsNullOrEmpty(searchTerm)) {
			appointments = repairAppointmentService.getAll();
		} else {
			appointments = repairAppointmentService.getBySearchString(searchTerm);
		}
		
		// sort by date - see compareTo() in RepairAppointmentModel
		Collections.sort(appointments);
		
		return new ResponseEntity<List<RepairAppointmentModel>>(appointments, HttpStatus.OK);
	}
	
	@CrossOrigin
	@RequestMapping(value = "/repairappointments/", method = RequestMethod.POST)
	public ResponseEntity<RepairAppointmentModel> createRepairAppointment(@RequestBody RepairAppointmentModel repairAppointment) {
		RepairAppointmentModel savedAppointment = repairAppointmentService.save(repairAppointment);
		return new ResponseEntity<RepairAppointmentModel>(savedAppointment, HttpStatus.OK);
	}
	
	@CrossOrigin
	@RequestMapping(value = "/repairappointments/{id}/", method = RequestMethod.GET)
	public ResponseEntity<RepairAppointmentModel> getRepairAppointment(@PathVariable Long id) {
		RepairAppointmentModel appointment = repairAppointmentService.getById(id);
		return new ResponseEntity<RepairAppointmentModel>(appointment, HttpStatus.OK);
	}
	
	@CrossOrigin
	@RequestMapping(value= "/repairappointments/{id}/", method = RequestMethod.PUT)
	public ResponseEntity<RepairAppointmentModel> updateRepairAppointment(@PathVariable Long id, @RequestBody RepairAppointmentModel repairAppointment) {
		RepairAppointmentModel updatedRepairAppointment = repairAppointmentService.save(repairAppointment);
		return new ResponseEntity<RepairAppointmentModel>(updatedRepairAppointment, HttpStatus.OK);
	}
	
	@CrossOrigin
	@RequestMapping(value = "/repairappointments/{id}/", method = RequestMethod.DELETE)
	public void deleteRepairAppointment(@PathVariable Long id) {
		repairAppointmentService.deleteById(id);
	}
	 
	private boolean searchTermIsNullOrEmpty(String searchTerm){
		return searchTerm == null || searchTerm.isEmpty();
	}

}
