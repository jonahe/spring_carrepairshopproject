package se.erikwiberg.carrepairshop.controllers;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import se.erikwiberg.carrepairshop.models.UserModel;

import se.erikwiberg.carrepairshop.services.UserService;


@RestController
@RequestMapping("/api")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@CrossOrigin
	@RequestMapping(value = "/users/", method = RequestMethod.GET)
	public ResponseEntity<List<UserModel>> getAllUsers(
			@RequestParam(name="searchFor", required=false) String searchFor
			) {
		List<UserModel> users;
		if(searchFor == null || searchFor.isEmpty()) {
			users = userService.getAll();
		} else {
			users = userService.getBySearchString(searchFor);
		}
		
		return new ResponseEntity<List<UserModel>>(users, HttpStatus.OK);
	}
	
	@CrossOrigin
	@RequestMapping(value = "/users/", method = RequestMethod.POST)
	public ResponseEntity<UserModel> createUser(@RequestBody UserModel user) {
		UserModel savedUser = userService.save(user);
		return new ResponseEntity<UserModel>(savedUser, HttpStatus.OK);
	}
	
	@CrossOrigin
	@RequestMapping(value = "/users/{id}/", method = RequestMethod.GET)
	public ResponseEntity<UserModel> getUser(@PathVariable Long id) {
		UserModel user = userService.getById(id);
		return new ResponseEntity<UserModel>(user, HttpStatus.OK);
	}
	
	@CrossOrigin
	@RequestMapping(value= "/users/{id}/", method = RequestMethod.PUT)
	public ResponseEntity<UserModel> updateUser(@PathVariable Long id, @RequestBody UserModel user) {
		UserModel updatedUser = userService.save(user);
		return new ResponseEntity<UserModel>(updatedUser, HttpStatus.OK);
	}
	
	@CrossOrigin
	@RequestMapping(value = "/users/{id}/", method = RequestMethod.DELETE)
	public void deleteUser(@PathVariable Long id) {
		userService.deleteById(id);
	}
	
	
	@CrossOrigin
	@RequestMapping(value = "/users/authenticate/", method = RequestMethod.GET)
	public ResponseEntity<UserModel> authenticateUserByEmail(@RequestParam(name="email", required=true) String email) {
		
		List<UserModel> users = userService.getAll();
		
		try {
			UserModel matchingUser = getMatchingUser(users, email);
			return new ResponseEntity<UserModel>(matchingUser, HttpStatus.OK);
			
		} catch(Exception e) {	
			return new ResponseEntity<UserModel>(new UserModel(), HttpStatus.FORBIDDEN);
		}

		
	}
	
	private UserModel getMatchingUser(List<UserModel> users, String email) throws Exception {
		
		for(UserModel user : users) {
			String userEmail =  user.getUserContactInfo().getEmail();
			if(email.equals(userEmail)){
				return user;
			}
		}
		throw new Exception("Authentification failed. No user with email of '" + email + "'");
	}

}
