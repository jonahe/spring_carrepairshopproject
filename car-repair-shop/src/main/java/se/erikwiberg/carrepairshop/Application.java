package se.erikwiberg.carrepairshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

@EnableAutoConfiguration
@SpringBootApplication
public class Application extends SpringBootServletInitializer { // /** inheritance. Added to make project deployable to Tomcat7 **/

	/** Added to make project deployable to Tomcat7 **/
	private static Class<Application> appClass = Application.class;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	/** Added to make project deployable to Tomcat7 **/
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		
		return builder.sources(appClass);
	}

}
