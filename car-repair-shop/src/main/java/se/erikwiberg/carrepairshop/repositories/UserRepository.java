package se.erikwiberg.carrepairshop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import se.erikwiberg.carrepairshop.models.UserModel;

public interface UserRepository extends JpaRepository<UserModel, Long> {}
