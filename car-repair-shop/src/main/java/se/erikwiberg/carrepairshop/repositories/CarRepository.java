package se.erikwiberg.carrepairshop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import se.erikwiberg.carrepairshop.models.CarModel;

public interface CarRepository extends JpaRepository<CarModel, Long> {}
