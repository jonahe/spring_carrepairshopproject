package se.erikwiberg.carrepairshop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import se.erikwiberg.carrepairshop.models.RepairAppointmentModel;

public interface RepairAppointmentRepository extends JpaRepository<RepairAppointmentModel, Long> {}
