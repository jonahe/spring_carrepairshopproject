package se.erikwiberg.carrepairshop.models;

public enum UserType {
	ADMIN, CUSTOMER
}
