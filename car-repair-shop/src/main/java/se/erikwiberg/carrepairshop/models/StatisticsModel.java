package se.erikwiberg.carrepairshop.models;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class StatisticsModel {
	private LinkedHashMap<String, Integer> entityToEntityCountMap;
	private Integer totalCount;
	
	public StatisticsModel() {
		entityToEntityCountMap = new LinkedHashMap<String, Integer>();
		totalCount = 0;
	}
	
	
	public LinkedHashMap<String, Integer> getEntityToEntityCountMap() {
		return entityToEntityCountMap;
	}

	public void setEntityToEntityCountMap(LinkedHashMap<String, Integer> entityToEntityCountMap) {
		setTotalCountFromEntityToEntityCountMap(entityToEntityCountMap);
		this.entityToEntityCountMap = entityToEntityCountMap;
	}


	public void putEntityNameEntityCountPair(String entityName, Integer count) {
		totalCount += count;
		entityToEntityCountMap.put(entityName, count);
	}
	
	public Integer getTotalCount(){
		return totalCount;
	}
	
	private void setTotalCountFromEntityToEntityCountMap(HashMap<String, Integer> entityToEntityCountMap) {
		totalCount = 0;
		
		for(Map.Entry<String, Integer> pair : entityToEntityCountMap.entrySet()){
			totalCount += pair.getValue();
		}
	}
}
