package se.erikwiberg.carrepairshop.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@SuppressWarnings("serial")
@Entity
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id",scope = CarModel.class)
public class UserModel implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Embedded
	private UserContactInfo userContactInfo = new UserContactInfo();

	@Enumerated(value=EnumType.STRING)
	private UserType userType;
	
	@OneToMany(cascade = CascadeType.DETACH, mappedBy = "owner")
	private List<CarModel> cars;
	
	
	@OneToMany(cascade = CascadeType.DETACH, mappedBy = "carOwner")
	private List<RepairAppointmentModel> repairAppointments;
	
	
	public UserModel() {}
	

	public Long getId() {
		return id;
	}
	
	
	public List<CarModel> getCars() {
		return cars;
	}

	public void setCars(List<CarModel> cars) {
		this.cars = cars;
	}
	

	public List<RepairAppointmentModel> getAppointments() {
		return repairAppointments;
	}


	public void setAppointments(List<RepairAppointmentModel> appointments) {
		this.repairAppointments = appointments;
	}


	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public UserContactInfo getUserContactInfo() {
		return userContactInfo;
	}

	public void setUserContactInfo(UserContactInfo userContactInfo) {
		this.userContactInfo = userContactInfo;
	}

}
