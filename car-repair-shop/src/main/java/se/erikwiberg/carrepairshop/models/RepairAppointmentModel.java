package se.erikwiberg.carrepairshop.models;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class RepairAppointmentModel implements Comparable<RepairAppointmentModel>{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private Date appointmentDate;
	private String problemDescription;
	
	@ManyToOne(cascade = CascadeType.DETACH) // CascadeType.DETACH solved problem with updating repair appointment - maybe because the info was duplicated through the owner in "car" varialbe?
	private UserModel carOwner;
	
	@ManyToOne(cascade = CascadeType.MERGE)
	private CarModel car;
	
	public RepairAppointmentModel() {};
	
	
	
	public Date getAppointmentDate() {
		return appointmentDate;
	}
	public void setAppointmentDate(Date appointmentDate) {
		this.appointmentDate = appointmentDate;
	}
	public String getProblemDescription() {
		return problemDescription;
	}
	public void setProblemDescription(String problemDescription) {
		this.problemDescription = problemDescription;
	}
	public UserModel getCarOwner() {
		return carOwner;
	}
	public void setCarOwner(UserModel carOwner) {
		this.carOwner = carOwner;
	}
	public CarModel getCar() {
		return car;
	}
	public void setCar(CarModel car) {
		this.car = car;
	}
	public Long getId() {
		return id;
	}



	@Override
	public int compareTo(RepairAppointmentModel appointment) {
		// sorted by DESC by date
		Date otherAppointmentDate = appointment.appointmentDate;
		if(appointmentDate.after(otherAppointmentDate)){
			return -1;
		} else if(appointmentDate.before(otherAppointmentDate)) {
			return 1;
		}
		return 0;
	}
	
	
	
}
