package se.erikwiberg.carrepairshop.services;

import java.util.List;

public interface Searchable<T> {
	List<T> getBySearchString(String searchTerm);
}
