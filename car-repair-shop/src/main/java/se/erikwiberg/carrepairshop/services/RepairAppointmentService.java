package se.erikwiberg.carrepairshop.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.erikwiberg.carrepairshop.models.RepairAppointmentModel;

@Service
public class RepairAppointmentService extends GenericBaseService<RepairAppointmentModel, Long> implements Searchable<RepairAppointmentModel> {

	@PersistenceContext
	@Autowired
	private EntityManager entityManager;

	@Override
	public List<RepairAppointmentModel> getBySearchString(String searchTerm) {
		String sqlQuery = "SELECT * FROM RepairAppointmentModel WHERE problemDescription LIKE '%" + searchTerm + "%'";
		Query query = entityManager.createNativeQuery(sqlQuery, RepairAppointmentModel.class);
		
		List<RepairAppointmentModel> results = (List<RepairAppointmentModel>) query.getResultList();
		return results;
	}
}
