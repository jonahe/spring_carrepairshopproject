package se.erikwiberg.carrepairshop.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.erikwiberg.carrepairshop.models.CarModel;

@Service
public class CarService extends GenericBaseService<CarModel, Long> implements Searchable<CarModel>{
	
	@PersistenceContext
	@Autowired
	private EntityManager entityManager;

	@Override
	public List<CarModel> getBySearchString(String searchTerm) {
		String sqlQuery = "SELECT * FROM CarModel WHERE (brand LIKE '%" + searchTerm + "%') OR (model LIKE '%" + searchTerm + "%')";
		Query query = entityManager.createNativeQuery(sqlQuery, CarModel.class);
		
		List<CarModel> results = (List<CarModel>) query.getResultList();
		return results;
	}
}
