package se.erikwiberg.carrepairshop.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.erikwiberg.carrepairshop.models.UserModel;

@Service
public class UserService extends GenericBaseService<UserModel, Long> implements Searchable<UserModel> {

	@PersistenceContext
	@Autowired
	private EntityManager entityManager;
	

	@Override
	public List<UserModel> getBySearchString(String searchTerm) {
		String sqlQuery = "SELECT * FROM UserModel WHERE (firstName LIKE '%" + searchTerm + "%') OR (lastName LIKE '%" + searchTerm + "%')";
		Query query = entityManager.createNativeQuery(sqlQuery, UserModel.class);
		
		List<UserModel> results = (List<UserModel>) query.getResultList();
		return results;
	}
}
