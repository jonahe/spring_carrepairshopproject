package se.erikwiberg.carrepairshop.services;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 * 
 * Provides CRUD functionality common to many services,
 * through an appropriately typed implementation of JpaRepository.
 *
 * @param <T> The type of entity that the service returns or deals with
 * @param <ID> The type used for id, must implement Serializable interface
 */
public abstract class GenericBaseService<T,ID extends Serializable> {
	
	@Autowired
	private JpaRepository<T, ID> repository;
	
	public List<T> getAll() {
		return repository.findAll();
	}
	
	public T save(T model) {
		return repository.saveAndFlush(model);
	}
	
	public T getById(ID id) {
		return repository.getOne(id);
	}
	
	public void deleteById(ID id) {
		repository.delete(id);
	}
	

}
