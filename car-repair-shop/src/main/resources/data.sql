
-- custom views for easier gathering of statistics

-- for appointments 
CREATE OR REPLACE VIEW view_appointments_info AS
	SELECT 
		appointment.appointmentDate as 'date', date_format(appointment.appointmentDate, '%Y/%m') as 'year/month', appointment.problemDescription as 'problem description',
		car.brand as 'car brand', car.model as 'car model',
		concat(usr.firstName, ' ', usr.lastName) as 'car owner', usr.userType as 'car owner user type'
	FROM RepairAppointmentModel appointment
	JOIN UserModel usr
	ON usr.id = appointment.carOwner_id
	JOIN CarModel car
	ON car.id = appointment.car_id 
 ORDER BY appointment.appointmentDate;
 
-- for users
CREATE OR REPLACE VIEW view_user_info AS
	SELECT 
	user.firstName as 'first name', user.lastName as 'last name', user.userType as 'user type'
    FROM UserModel user;

-- for cars
CREATE OR REPLACE VIEW view_car_info AS
	SELECT 
	car.brand, car.model, cast(car.productionYear as char(20)) as 'production year', car.propulsionTechnology as 'propulsion technology',
    concat(user.firstName, ' ', user.lastName) as 'owner name', user.userType as 'car owner user type'
	FROM CarModel car
	JOIN UserModel user
	ON user.id = car.ownerId;
	
-- create test data

-- users

INSERT INTO UserModel (email, firstName, lastName, phoneNumber, userType)
	VALUES ('admin@admin.se', 'Admin', 'Adminsson', '325252363262', 'ADMIN');

INSERT INTO UserModel (email, firstName, lastName, phoneNumber, userType)
	VALUES ('johan@customer.se', 'Johan', 'Johansson', '325252363262', 'CUSTOMER');
	
INSERT INTO UserModel (email, firstName, lastName, phoneNumber, userType)
	VALUES ('stina@customer.se', 'Stina', 'Johansson', '32522352353', 'CUSTOMER');
	
INSERT INTO UserModel (email, firstName, lastName, phoneNumber, userType)
	VALUES ('emma@customer.se', 'Emma', 'Emmasson', '311632651', 'CUSTOMER');
	
INSERT INTO UserModel (email, firstName, lastName, phoneNumber, userType)
	VALUES ('peter@customer.se', 'Peter', 'Halmqvist', '511632259', 'CUSTOMER');
	
INSERT INTO UserModel (email, firstName, lastName, phoneNumber, userType)
	VALUES ('najib@customer.se', 'Najib', 'Rahia', '722734331', 'CUSTOMER');
	
-- cars
	
INSERT INTO CarModel (brand, model, productionYear, propulsionTechnology, ownerId)
	VALUES ('Volvo','V70', 1994,'gasoline', 2);
	
INSERT INTO CarModel (brand, model, productionYear, propulsionTechnology, ownerId)
	VALUES ('Volvo','240', 1979,'diesel',2);
	
INSERT INTO CarModel (brand, model, productionYear, propulsionTechnology, ownerId)
	VALUES ('Saab','9-5', 1998,'gasoline',3);
	
INSERT INTO CarModel (brand, model, productionYear, propulsionTechnology, ownerId)
	VALUES ('Saab','9000', 1986,'diesel',4);
	
INSERT INTO CarModel (brand, model, productionYear, propulsionTechnology, ownerId)
	VALUES ('Opel','Astra', 1992,'gasoline',5);
	
INSERT INTO CarModel (brand, model, productionYear, propulsionTechnology, ownerId)
	VALUES ('Tesla','Cool', 2016,'electric',6);
	
-- appointments

-- johan, has cars 1,2
INSERT INTO RepairAppointmentModel (appointmentDate, problemDescription, car_id, carOwner_id)
	VALUES ('2012-04-02 12:00:00','broken window', 2, 2);
	
INSERT INTO RepairAppointmentModel (appointmentDate, problemDescription, car_id, carOwner_id)
	VALUES ('2013-02-22 12:00:00','broken door', 2, 2);
	
INSERT INTO RepairAppointmentModel (appointmentDate, problemDescription, car_id, carOwner_id)
	VALUES ('2016-02-02 12:00:00','engine not starting', 1, 2);

INSERT INTO RepairAppointmentModel (appointmentDate, problemDescription, car_id, carOwner_id)
	VALUES ('2016-04-16 12:00:00','engine not starting.. again.', 1, 2);
	
-- stina, has car 3
	
INSERT INTO RepairAppointmentModel (appointmentDate, problemDescription, car_id, carOwner_id)
	VALUES ('2013-02-01 12:00:00','Noise from transmission', 3, 3);
	
INSERT INTO RepairAppointmentModel (appointmentDate, problemDescription, car_id, carOwner_id)
	VALUES ('2016-02-18 12:00:00','Small crack in passenger side window', 3, 3);

INSERT INTO RepairAppointmentModel (appointmentDate, problemDescription, car_id, carOwner_id)
	VALUES ('2016-04-08 12:00:00','Driver side headlight is out', 3, 3);


-- emma, has car 4

INSERT INTO RepairAppointmentModel (appointmentDate, problemDescription, car_id, carOwner_id)
	VALUES ('2012-03-15 12:00:00','Radio antenna broken by huligans', 4, 4);
	
INSERT INTO RepairAppointmentModel (appointmentDate, problemDescription, car_id, carOwner_id)
	VALUES ('2013-01-22 12:00:00','Back seat not soft enough', 4, 4);
	
INSERT INTO RepairAppointmentModel (appointmentDate, problemDescription, car_id, carOwner_id)
	VALUES ('2016-09-02 12:00:00','Routine control', 4, 4);

INSERT INTO RepairAppointmentModel (appointmentDate, problemDescription, car_id, carOwner_id)
	VALUES ('2016-03-16 12:00:00','Damaged lock on passanger side door', 4, 4);
	
INSERT INTO RepairAppointmentModel (appointmentDate, problemDescription, car_id, carOwner_id)
	VALUES ('2016-01-02 12:00:00','Routine control', 4, 4);

INSERT INTO RepairAppointmentModel (appointmentDate, problemDescription, car_id, carOwner_id)
	VALUES ('2016-12-16 12:00:00','Damaged lock on driver side door', 4, 4);
	
INSERT INTO RepairAppointmentModel (appointmentDate, problemDescription, car_id, carOwner_id)
	VALUES ('2016-08-02 12:00:00','Missing one wheel', 4, 4);

INSERT INTO RepairAppointmentModel (appointmentDate, problemDescription, car_id, carOwner_id)
	VALUES ('2016-12-16 12:00:00','Smells wierd', 4, 4);
	
	
-- peter, has car 5

INSERT INTO RepairAppointmentModel (appointmentDate, problemDescription, car_id, carOwner_id)
	VALUES ('2012-05-15 12:00:00','Radio antenna broken by huligans', 5, 5);
	
INSERT INTO RepairAppointmentModel (appointmentDate, problemDescription, car_id, carOwner_id)
	VALUES ('2013-03-22 12:00:00','Back seat not soft enough', 5, 5);
	
INSERT INTO RepairAppointmentModel (appointmentDate, problemDescription, car_id, carOwner_id)
	VALUES ('2016-11-02 12:00:00','Routine control', 5, 5);

INSERT INTO RepairAppointmentModel (appointmentDate, problemDescription, car_id, carOwner_id)
	VALUES ('2016-02-16 12:00:00','Damaged lock on passanger side door', 5, 5);
	
INSERT INTO RepairAppointmentModel (appointmentDate, problemDescription, car_id, carOwner_id)
	VALUES ('2016-01-02 12:00:00','Missing one wheel', 5, 5);

INSERT INTO RepairAppointmentModel (appointmentDate, problemDescription, car_id, carOwner_id)
	VALUES ('2016-06-19 12:00:00','Smells wierd', 5, 5);
	
	
-- Najib has car 6
	
INSERT INTO RepairAppointmentModel (appointmentDate, problemDescription, car_id, carOwner_id)
	VALUES ('2016-05-15 12:00:00','Too quiet', 6, 6);
	
INSERT INTO RepairAppointmentModel (appointmentDate, problemDescription, car_id, carOwner_id)
	VALUES ('2016-03-22 12:00:00','Battery is dead', 6, 6);
	
INSERT INTO RepairAppointmentModel (appointmentDate, problemDescription, car_id, carOwner_id)
	VALUES ('2016-11-02 12:00:00','Self-driving feature broken', 6, 6);

INSERT INTO RepairAppointmentModel (appointmentDate, problemDescription, car_id, carOwner_id)
	VALUES ('2016-02-16 12:00:00','Driver seat too comfortable.', 6, 6);
	
INSERT INTO RepairAppointmentModel (appointmentDate, problemDescription, car_id, carOwner_id)
	VALUES ('2016-01-02 12:00:00','Too cool', 6, 6);

INSERT INTO RepairAppointmentModel (appointmentDate, problemDescription, car_id, carOwner_id)
	VALUES ('2016-06-19 12:00:00','Routine control', 6, 6);