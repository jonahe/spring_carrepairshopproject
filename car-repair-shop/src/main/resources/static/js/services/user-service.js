/**
 *  Service for CRUD operations for users
 */

angular.module('repairshop')
	.service('UserService', ['$http', '$rootScope', function($http, $rootScope) {

		
		var baseUrl = $rootScope.API_baseUrl + "users/";
		
		this.getAllUsers = function() {
			return $http.get(baseUrl);
		};
		
		this.getUserById = function(id) {
			return $http.get(baseUrl + id + "/");
		};
		
		this.createUser = function(jsonUser) {
			return $http.post(baseUrl, jsonUser);
		}
		
		this.updateUser = function(jsonUser) {
			return $http.put(baseUrl + jsonUser.id + "/" , jsonUser);
		}
		
		this.deleteUserById = function(id) {
			return $http.delete(baseUrl + id + "/");
		}
	}]);