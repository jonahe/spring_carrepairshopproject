/**
 *  Service for CRUD and authentication operations for users
 */

angular.module('repairshop')
	.service('LoginService', ['$http', '$rootScope', '$window', function($http, $rootScope, $window) {
	
	var self = this;
	
	self.loggedInUser = {};
	
	
	loadLoggedInUserFromLocalStorage();

	
	self.isLoggedIn = function() {
		return sessionStorage.userAsString ? true : false;
	}
	

	self.logOut = function() {
		loggedInUser = {};
		clearSessionStorage();
	}
	
	self.authenticateByEmail = function(email) {
		return $http.get( $rootScope.API_baseUrl + "users/authenticate/", {"params": {"email" : email}})
			.then(storeUserInController)
			.then(takeReturnedUserAndStoreInLocalStorage);
	}


	self.redirectToLoginPageIfNotLoggedIn = function() {
		if(!self.isLoggedIn()) {
			console.log("user not logged in. redirecting to login page");
			$window.location.href = $rootScope.baseUrl;
		}
	}
	
	this.loggedInAsAdmin = function () {
		return self.loggedInUser.userType === "ADMIN";
	}
	
	this.loggedInAsCustomer = function() {
		return self.loggedInUser.userType === "CUSTOMER";
	}
	
	this.getUserObjectIfExists = function() {
		if(self.isLoggedIn()) {
			return self.loggedInUser;
		}
		return false;
		
	}
	

	function storeUserInController(response) {
		self.loggedInUser = response.data;
		return response.data;
	}

	function takeReturnedUserAndStoreInLocalStorage(user) {
		
		if(typeof(Storage) !== "undefined") {
			
			// sessionStorage can only store key-value pairs. not objects
			sessionStorage.userAsString = JSON.stringify(user);
			sessionStorage.userType =user.userType;
			sessionStorage.userId = user.id;
			
		} else {
			throw new Error("Can't store user info. Browser doesn't support HTML5 sessionStorage");
		}
		
		return user; // pass on through
	}
	
	function loadLoggedInUserFromLocalStorage(){
		if(typeof(Storage) !== "undefined") {
			
			if(sessionStorage.userAsString) {
				self.loggedInUser = JSON.parse(sessionStorage.userAsString);
			}
			
		}
	}
	
	function clearSessionStorage() {
		if(typeof(Storage) !== "undefined") {
			delete sessionStorage.userAsString;
			delete sessionStorage.userType;
			delete sessionStorage.userId;
		}
	}
	
	
// end of service..
}]);