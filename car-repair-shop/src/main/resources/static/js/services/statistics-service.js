/**
 *  Service for getting statistics
 */

angular.module('repairshop')
	.service('statisticsService', ['$http', '$rootScope', function($http, $rootScope) {

		var baseUrl = $rootScope.API_baseUrl + "statistics/";
		
		var service = this;
		
		this.getCarStatistics = function(columnName) {
			var url = baseUrl + "cars/"
			return service.getStatisticsByUrlAndColumnName(url, columnName);
		};
		
		this.getRepairAppointmentStatistics = function(columnName) {
			var url = baseUrl + "repairappointments/"
			return service.getStatisticsByUrlAndColumnName(url, columnName);
		};
		
		this.getUserStatistics = function(columnName) {
			var url = baseUrl + "users/"
			return service.getStatisticsByUrlAndColumnName(url, columnName);
		};
		
		
		this.getStatisticsByUrlAndColumnName = function(url, columnName){
			return $http.get(url, {params: {"columnName" : columnName}});
		}
	}]);