/**
 *  Service for CRUD operations for cars
 */

angular.module('repairshop')
	.service('CarService', ['$http','$rootScope', function($http, $rootScope) {

		
		var baseUrl = $rootScope.API_baseUrl + "cars/";
		
		this.getAllCars = function() {
			return $http.get(baseUrl);
		};
		
		this.getCarById = function(id) {
			return $http.get(baseUrl + id + "/");
		};
		
		this.createCar = function(jsonCar) {
			return $http.post(baseUrl, jsonCar);
		}
		
		this.updateCar = function(jsonCar) {
			return $http.put(baseUrl + jsonCar.id + "/" , jsonCar);
		}
		
		this.deleteCarById = function(id) {
			return $http.delete(baseUrl + id + "/");
		}
	}]);