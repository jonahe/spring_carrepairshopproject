/**
 *  Service for CRUD operations for repair appointments
 */

angular.module('repairshop')
	.service('RepairAppointmentService', ['$http','$rootScope', function($http, $rootScope) {

		
		var baseUrl = $rootScope.API_baseUrl + "repairappointments/";
		
		this.getAllAppointments = function() {
			return $http.get(baseUrl);
		};
		
		this.getAppointmentById = function(id) {
			return $http.get(baseUrl + id + "/");
		};
		
		this.createAppointment = function(jsonAppointment) {
			return $http.post(baseUrl, jsonAppointment);
		}
		
		this.updateAppointment = function(jsonAppointment) {
			return $http.put(baseUrl + jsonAppointment.id + "/" , jsonAppointment);
		}
		
		this.deleteAppointmentById = function(id) {
			return $http.delete(baseUrl + id + "/");
		}
	}]);