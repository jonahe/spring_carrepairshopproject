// wrapped in a closure, good habit. (encapsulation?)
(function() {
	var app = angular.module('repairshop', ['ngRoute', 'customFilterModule']); // name  [dependencies] , name is referenced in html tag
	

	// ROUTING 
	app.config( function( $routeProvider) {

		$routeProvider
		.when('/', {
			title: 'Home',
			activeTab: 'Home',
			templateUrl: 'pages/login/login.html', 
			controller: 'loginController'
		})
		// cars
		.when('/cars', {
			title: 'Cars',
			activeTab: 'Cars',
			templateUrl: 'pages/cars/show-all-cars.html', 
			controller: 'carController'
				
		}).when('/cars/create-new', {
			title: 'Create new car',
			activeTab: 'Cars',
			templateUrl: 'pages/cars/show-edit-create-car.html',
			controller: 'editOrCreateCarController'
				
		}).when('/cars/:id', {
			title: 'View or update car',
			activeTab: 'Cars',
			templateUrl: 'pages/cars/show-edit-create-car.html',
			controller: 'editOrCreateCarController'
		})
		
		// appointments
		.when('/repair-appointments', {
			title: 'Appointments',
			activeTab: 'Appointments',
			templateUrl: 'pages/repair-appointments/show-all-repair-appointments.html', 
			controller: 'repairAppointmentController'
				
		}).when('/repair-appointments/create-new', {
			title: 'Create new appointment',
			activeTab: 'Appointments',
			templateUrl: 'pages/repair-appointments/show-edit-create-repair-appointment.html', 
			controller: 'editOrCreateRepairAppointmentController'
				
		}).when('/repair-appointments/:id', {
			title: 'View or update appointment',
			activeTab: 'Appointments',
			templateUrl: 'pages/repair-appointments/show-edit-create-repair-appointment.html', 
			controller: 'editOrCreateRepairAppointmentController'
		})
		
		
		// users
		.when('/users', {
			title: 'Users',
			activeTab: 'Users',
			templateUrl: 'pages/users/show-all-users.html', 
			controller: 'userController'
				
		}).when('/users/create-new', {
			title: 'Create new user',
			activeTab: 'Users',
			templateUrl: 'pages/users/show-edit-create-user.html',
			controller: 'editOrCreateUserController'
				
		}).when('/users/:id', {
			title: 'View or update user',
			activeTab: 'Users',
			templateUrl: 'pages/users/show-edit-create-user.html', 
			controller: 'editOrCreateUserController'
				
		})
		
		// statistics
		.when('/statistics', {
			title: 'Statistics',
			activeTab: 'Statistics',
			templateUrl: 'pages/statistics/statistics.html', 
			controller: 'statisticsController'
		})
		
		// otherwise redirect to home
		.otherwise("/")
	});
	
	
	// middle-ware
	
	// make title names and other functionality accessible through $rootScope
	app.run(['$rootScope', 'LoginService', function($rootScope, LoginService) {
	    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
	        
	    	$rootScope.title = current.$$route.title;

	    	// early abort, if not already on login page
	    	if(current.$$route.title !== 'Home') {
	    		LoginService.redirectToLoginPageIfNotLoggedIn();
	    	}
	    	
	        $rootScope.baseUrl = "/repairshop/#/";
	        $rootScope.API_baseUrl = "/repairshop/api/";
	        
	        $rootScope.activeTab = current.$$route.activeTab;
	        
	        
	        // make it easy for each page/route to see what status the user has
	        $rootScope.loggedInAsAdmin = LoginService.loggedInAsAdmin();
	        // easy access to customer object
	        $rootScope.loggedInAsUser = LoginService.getUserObjectIfExists() || "not logged in";
	        
	        
	        $rootScope.getDateFromMillis = function(millis) {
	    		var date = new Date(millis);
	    		date.setHours(12);
	    		date.setMinutes(0);
	    		date.setSeconds(0);
	    		return date.toDateString() + " " + date.getHours() + ":" + "00";
	    	}
	        
	        
	        $rootScope.basicErrorHandling = function(error) {
	    		
	    		var statusCode = error.status;
	    		var method = error.config.method;
	    		var url = error.config.url;
	    		
	    		console.log(url);
	    		
	    		// user tried to delete something..
	    		if(statusCode == "500" && method == "DELETE"){
	    			
	    			var entity = "";
	    			var solution = "";
	    			
	    			if(url.includes('cars')) {
	    				entity = "car";
		    			solution = "delete associated appointments";
	    			} else if (url.includes('users')) {
	    				entity = "user"
	    				solution = "delete associated appointments and (then) cars";
	    			}

	    			alert(
	    					error.statusText 
	    					+ "! \nIt seems you tried to delete a " + entity + " with connections to something else."
	    					+ "\nYou may first have to " + solution + "."
	    			)
	    			
	    		} else {			
	    			alert("Whoops! This wasn't here when I tested.. " + error.statusText  + " (status " + error.status +")\n" + error.message || url);
	    		}
	    	}
	        
	    });
	}]);

})(); // run at once

