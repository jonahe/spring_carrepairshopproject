angular.module('repairshop')
.controller('editOrCreateUserController', ['$rootScope', '$scope', '$routeParams', '$window','UserService', function($rootScope, $scope, $routeParams, $window, UserService) {
	
	$scope.successFeedback = null;
	$scope.buttonText = "save changes";
	$scope.user = {};
	$scope.userTypes = ["ADMIN", "CUSTOMER"];
	
	$scope.resetSuccessFeedback = function() {
		$scope.successFeedback = null;
	}
	
	
	// in update mode
	if($routeParams.id){
		
		$scope.buttonText = "save changes";
		
		UserService.getUserById($routeParams.id)
		.then( 
			function(response) {
				$scope.user = response.data;
			},
			$rootScope.basicErrorHandling
		);
			
	// in creation mode		
	} else {
		$scope.buttonText = "create user";
	}
	
	// saves or updates depending on whether the user id is set
	$scope.saveUser = function() {
		
		// update
		if($scope.user.id) {
			UserService.updateUser($scope.user)
			.then(
					function(response) {
						$scope.user = response.data;
						$scope.successFeedback  = "Changes were successfully saved";
					},
					$rootScope.basicErrorHandling
			);
		} else {
			// create new
			UserService.createUser($scope.user)
			.then(
					function(response) {
						$scope.user = response.data;
						$scope.successFeedback  = "User was successfully created - REDIRECTING to view/update page";
						
						setTimeout( function() {
							$window.location.href = $rootScope.baseUrl + "users/" + response.data.id;
							
						}, 2000);
					},
					$rootScope.basicErrorHandling
			);
		}
		
	};
	
	
}]);
