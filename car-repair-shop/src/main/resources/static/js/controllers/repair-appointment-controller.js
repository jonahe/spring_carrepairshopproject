angular.module('repairshop')
.controller('repairAppointmentController', ['$rootScope', '$scope', '$routeParams','RepairAppointmentService', function($rootScope, $scope, $routeParams, RepairAppointmentService) {

	$scope.repairAppointments = [];
	
	
	getAndLoadAllRepairAppointments();
	

	$scope.deleteAppointmentById = function(id) {
		
		RepairAppointmentService.deleteAppointmentById(id)
		.then(
				function(response) {
					getAndLoadAllRepairAppointments(); // reload updated list
					
				},
				$rootScope.basicErrorHandling
			);
	};
	

	function getAndLoadAllRepairAppointments() {
		
		RepairAppointmentService.getAllAppointments().then(function(response) {
			
			var appointmentsArray = response.data;
			
			appointmentsArray = fillInMissingInfo(appointmentsArray);
			
			$scope.repairAppointments = appointmentsArray;
		}, $rootScope.basicErrorHandling);		
	}
	
	function fillInMissingInfo(appointmentsArray) {
		//work-around for weird effect of using @JsonIdentityInfo in java back-end to avoid circular references (bi-directional bindning)
		// some car objects only contain references by id to owner/user objects, so here we get the full object and insert it so that the owner info is available in all cars
		
		var tempOwnerID_to_ownerContactInfo = {};
		for(var i = 0; i < appointmentsArray.length; i++) {
			var appointment = appointmentsArray[i];
			// owner is a full object, with full info, save for later
			if(appointment.carOwner.id){
				var ownerContactInfo = appointment.carOwner.userContactInfo;
				tempOwnerID_to_ownerContactInfo[appointment.carOwner.id] = ownerContactInfo;		
			// owner just has reference id, fill in info
			} else {
				var referenceId = appointment.carOwner;
				appointmentsArray[i].carOwner = {};
				appointmentsArray[i].carOwner.id = referenceId;
				appointmentsArray[i].carOwner['userContactInfo'] = tempOwnerID_to_ownerContactInfo[referenceId];
			}
		}
		
		return appointmentsArray;
	}
}]);