angular.module('repairshop')
.controller('statisticsController', ['$rootScope', '$scope', '$routeParams', 'statisticsService', function($rootScope, $scope, $routeParams, statisticsService) {

	
	
	$scope.validCarColumnNames = ['brand', 'model', 'production year', 'propulsion technology', 'owner name', 'car owner user type'];
	$scope.validRepairAppointmentColumnNames = ['year/month', 'car owner','car brand','car model','car owner user type'];
	$scope.validUserColumnNames = ['user type', 'first name', 'last name'];
	
	$scope.carStatistics = {};
	$scope.repairAppointmentStatistics = {};
	$scope.userStatistics = {};
	
	
	// pre-select values
	$scope.selectedCarStatisticsColumn = "brand";
	$scope.selectedRepairAppointmentStatisticsColumn = "year/month";
	$scope.selectedUserStatisticsColumn = "user type";
	
	

	// run -> populate the page first time
	getCarStatistics(); 
	getRepairAppointmentStatistics();
	getUserStatistics();
	
	function getCarStatistics(){
		
		if($scope.selectedCarStatisticsColumn) {
			
			statisticsService.getCarStatistics($scope.selectedCarStatisticsColumn)
			.then(
					function(response) {
						$scope.carStatistics = response.data;
					}, 
					$rootScope.basicErrorHandling
			);			
		}
	}
	
	function getRepairAppointmentStatistics(){

		if($scope.selectedRepairAppointmentStatisticsColumn) {
			
			statisticsService.getRepairAppointmentStatistics($scope.selectedRepairAppointmentStatisticsColumn)
			.then(
					function(response) {
						$scope.repairAppointmentStatistics = response.data;
					}, 
					$rootScope.basicErrorHandling
			);			
		}
	}
	
	function getUserStatistics(){
		
		if($scope.selectedUserStatisticsColumn) {
			
			statisticsService.getUserStatistics($scope.selectedUserStatisticsColumn)
			.then(
					function(response) {
						$scope.userStatistics = response.data;
					}, 
					$rootScope.basicErrorHandling
			);			
		}
	}
	
	// make functions available from html
	$scope.getCarStatistics = getCarStatistics;
	$scope.getRepairAppointmentStatistics = getRepairAppointmentStatistics;
	$scope.getUserStatistics = getUserStatistics;
	
	$scope.getPercentageValue = function(part, whole) {
		var percentage = (part / whole) * 100;
		return percentage.toFixed(1);
	}
	

}]);