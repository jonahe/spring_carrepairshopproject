angular.module('repairshop')
.controller('editOrCreateCarController', ['$rootScope', '$scope', '$routeParams', '$window','CarService', 'UserService', function($rootScope, $scope, $routeParams, $window, CarService, UserService) {
	
	$scope.successFeedback = null;
	$scope.buttonText = "save changes";
	$scope.car = {"brand":"","model":"","productionYear":"","propulsionTechnology":""};
	
	$scope.users = [];
	$scope.selectedUser;
	
	$scope.editMode = inEditMode(); // used to disable changing of owner (messes up existing appointments)
	
	getAllUsers();
	
	function getAllUsers() {
		UserService.getAllUsers().then( function(response) {
			$scope.users = response.data;
		}, 	$rootScope.basicErrorHandling);
	}
	
	
	function selectUserFromLoadedCar(car) {
		$scope.selectedUser = car.owner;
	}
	
	function selectUserFromLoggedInAsUser(){
		$scope.selectedUser = $rootScope.loggedInAsUser;
	}
	
	$scope.resetSuccessFeedback = function() {
		$scope.successFeedback = null;
	}
	
	
	if(inEditMode()){
		
		$scope.buttonText = "save changes";
		
		CarService.getCarById($routeParams.id)
		.then( 
			function(response) {
				$scope.car = response.data;
				selectUserFromLoadedCar($scope.car);
			},
			$rootScope.basicErrorHandling
		);
			
	// in creation mode		
	} else {
		$scope.buttonText = "create car";
		if(!$rootScope.loggedInAsAdmin){
			selectUserFromLoggedInAsUser(); // preselect the only option (only option because of filtering in ng-options)
		}
	}
	
	
	
	
	// saves or updates depending on whether the car id is set
	$scope.saveCar = function() {
		
		// update
		if($scope.car.id) {
			CarService.updateCar($scope.car)
			.then(
					function(response) {
						$scope.car = response.data;
						$scope.successFeedback  = "Changes were successfully saved";
					},
					$rootScope.basicErrorHandling
			);
		} else {
			// create new
			$scope.car.owner = $scope.selectedUser;
			
			CarService.createCar($scope.car)
			.then(
					function(response) {
						$scope.car = response.data;
						$scope.successFeedback  = "Car was successfully created - REDIRECTING to view/edit page";
						
						setTimeout( function() {
							$window.location.href = $rootScope.baseUrl + "cars/" + response.data.id;
							
						}, 2000);
					},
					$rootScope.basicErrorHandling
			);
		}
		
	};
	
	function inEditMode(){
		return $routeParams.id ? true : false;
	}
	
	
}]);
