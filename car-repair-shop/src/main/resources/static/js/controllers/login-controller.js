angular.module('repairshop')
.controller('loginController', ['$rootScope','$scope', 'LoginService', function($rootScope, $scope, LoginService) {
	
	$scope.email;
	
	$scope.loggedInUser;
	
	$scope.isLoggedIn = LoginService.isLoggedIn();
	
	
	if(LoginService.isLoggedIn()){
		$scope.loggedInUser = LoginService.getUserObjectIfExists();
	}
	
	$scope.submit = function() {
		
		LoginService.authenticateByEmail($scope.email)
		.then( function(user) {
			
			$scope.loggedInUser = user;
			$scope.isLoggedIn = true;
			if(user.userType === "ADMIN") {
				$rootScope.loggedInAsAdmin = true;				
			}
			
		}, function(error) {
			alert("Login failed. No user with email of '" + $scope.email +"'?");
			console.log(error);
		});
	}
	
	$scope.logOut = function() {
		LoginService.logOut();
		$scope.isLoggedIn = false;
		$scope.loggedInUser = null;
		$rootScope.loggedInAsAdmin = false;
	}
	
}]);