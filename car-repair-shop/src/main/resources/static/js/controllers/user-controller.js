angular.module('repairshop')
.controller('userController', ['$rootScope', '$scope', '$routeParams','UserService', function($rootScope, $scope, $routeParams, UserService) {

	$scope.selectedTab = "users";
	$scope.users = [];

	getAllUsers();
	
	$scope.createUser = function() {
		
		UserService.createUser(jsonUser)
		.then(
				function(response) {
					$scope.users.push(response.data); // add the car to the list
				},
				$rootScope.basicErrorHandling
			);
	};

	$scope.deleteUserById = function(id) {
		
		UserService.deleteUserById(id)
		.then(
				function(response) {
					getAllUsers(); // refresh list
					
				},
				$rootScope.basicErrorHandling
			);
	};
	
	$scope.getDateFromMillis = function(millis) {
		var date = new Date(millis);
		return date.toDateString() + " " + date.getHours() + ":" + date.getMinutes();
	}
	
	function getAllUsers() {
		UserService.getAllUsers().then(function(response) {
			
			var userArray = response.data;
			
			$scope.users = userArray;
			
		});		
	}
	
}]);