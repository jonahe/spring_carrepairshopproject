angular.module('repairshop')
.controller('carController', ['$rootScope', '$scope', '$routeParams','CarService', function($rootScope, $scope, $routeParams, CarService) {

	$scope.selectedTab = "cars";
	$scope.cars = [];
	
	
	getAllCars();
	
	
	$scope.createCar = function() {
		
		CarService.createCar(jsonCar)
		.then(
				function(response) {
					$scope.cars.push(response.data); // add the car to the list
				},
				$rootScope.basicErrorHandling
			);
	};

	$scope.deleteCarById = function(id) {
		
		CarService.deleteCarById(id)
		.then(
				function(response) {
					getAllCars(); // refresh list
					
				},
				$rootScope.basicErrorHandling
			);
	};
	
	function getAllCars() {
		CarService.getAllCars().then(
				// on success
				function(response) {
					
					var carArray = response.data;
					
					carArray = fillInMissingInfo(carArray);
					
					$scope.cars = carArray;
					
				}, // on error
				$rootScope.basicErrorHandling
		);		
	}
	
	function fillInMissingInfo(carArray) {
		//work-around for weird effect of using @JsonIdentityInfo in java back-end to avoid circular references (bi-directional bindning)
		// some car objects only contain references by id to owner/user objects, so here we get the full object and insert it so that the owner info is available in all cars
		
		var tempOwnerID_to_ownerContactInfo = {};
		for(var i = 0; i < carArray.length; i++) {
			var car = carArray[i];
			// owner is a full object, with full info, save for later
			if(car.owner.id){
				var ownerContactInfo = car.owner.userContactInfo;
				tempOwnerID_to_ownerContactInfo[car.owner.id] = ownerContactInfo;		
			// owner just has reference id, fill in info
			} else {
				var referenceId = car.owner;
				carArray[i].owner = {};
				carArray[i].owner.id = referenceId;
				carArray[i].owner['userContactInfo'] = tempOwnerID_to_ownerContactInfo[referenceId];
			}
		}
		
		return carArray;
	}
}]);