angular.module('repairshop')
.controller('editOrCreateRepairAppointmentController', ['$rootScope', '$scope', '$routeParams', '$window','RepairAppointmentService', 'UserService', function($rootScope, $scope, $routeParams, $window, RepairAppointmentService, UserService) {
	
	$scope.successFeedback = null;
	$scope.buttonText = "save changes";
	$scope.appointment = {};
	$scope.formattedAppointmentDate;
	
	$scope.users = [];
	$scope.selectedUser; 
	$scope.selectedCar;
	
	getAndLoadUsersForSelectBox();
	

	if(inUpdateMode()){
		
		$scope.buttonText = "save changes";
		
		getAndLoadDataFromAppointment();
		
			
	// in creation mode		
	} else {
		$scope.buttonText = "create appointment";
		
		presetDateToTomorrowAt12();
		
	}
	
	
	$scope.resetSuccessFeedback = function() {
		$scope.successFeedback = null;
	}
	
	// saves or updates depending on whether the appointment id is set
	$scope.saveAppointment = function() {
		
		prepareForSaveOrUpdate();

		if(inUpdateMode()) {
			
			uppdateAppointment();

		} else {
			createAppointment();
			
		}
		
	};
	
	function getAndLoadDataFromAppointment() {
		RepairAppointmentService.getAppointmentById($routeParams.id)
		.then( 
			function(response) {
				$scope.appointment = response.data;
				// make date number a Date object
				$scope.formattedAppointmentDate  = new Date(0 + $scope.appointment.appointmentDate);

				$scope.selectedUser = $scope.appointment.carOwner;
				$scope.selectedCar = $scope.appointment.car;
				
			},
			$rootScope.basicErrorHandling
		);
	}
	
	function getAndLoadUsersForSelectBox() {
		UserService.getAllUsers()
		.then( function(response) {
			$scope.users = response.data;
		}, $rootScope.basicErrorHandling
		);		
	}
	
	function prepareForSaveOrUpdate(){
		$scope.appointment.carOwner = $scope.selectedUser;
		$scope.appointment.car = $scope.selectedCar;
		$scope.appointment.car.owner = $scope.selectedUser.id;
		
		// convert back to milliseconds
		$scope.appointment.appointmentDate = $scope.formattedAppointmentDate.getTime(); 
		// all appointments are 12:00			
	}
	
	function uppdateAppointment() {
		RepairAppointmentService.updateAppointment($scope.appointment)
		.then(
				function(response) {
					$scope.appointment = response.data;
					$scope.successFeedback  = "Changes were successfully saved";
				},
				$rootScope.basicErrorHandling
		);				
	}
	
	function createAppointment() {
		RepairAppointmentService.createAppointment($scope.appointment)
		.then(
				function(response) {
					$scope.appointment = response.data;
					$scope.successFeedback  = "Appointment was created - REDIRECTING to view/update page";
					
					setTimeout( function() {
						$window.location.href = $rootScope.baseUrl + "repair-appointments/" + response.data.id;
						
					}, 2000);
				},
				$rootScope.basicErrorHandling
		);	
	}
	
	function presetDateToTomorrowAt12() {
		var tomorrowAt12 = new Date();
		tomorrowAt12.setDate(tomorrowAt12.getDate() +1);
		tomorrowAt12.setHours(12);
		tomorrowAt12.setMinutes(0);
		
		$scope.formattedAppointmentDate = tomorrowAt12;			
	}
	
	
	function inUpdateMode(){
		return $routeParams.id && true;
	}
	
	
	
}]);
