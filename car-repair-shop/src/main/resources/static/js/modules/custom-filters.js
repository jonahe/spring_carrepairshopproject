(function() { // IIFE
	var customFilterModule = angular.module('customFilterModule', []);
	
	// custom filters (that work on and return arrays)
	
	customFilterModule.filter('shallowMatchesForCarSearchTerm', [function() {
		return function(carArray, searchTerm) {
			return getObjectsWithSelectedValuesThatMatchSearchTerm( // function composition FTW?
					carArray, 
					getRelevantValuesFromCar, 
					searchTerm);
		}
	}]);
	
	customFilterModule.filter('shallowMatchesForAppointmentSearchTerm', [function() {
		return function(appointmentArray, searchTerm) {
			return getObjectsWithSelectedValuesThatMatchSearchTerm(
					appointmentArray, 
					getRelevantValuesFromAppointment, 
					searchTerm);
		}
	}]);
	
	customFilterModule.filter('shallowMatchesForUserSearchTerm', [function() {
		return function(userArray, searchTerm) {	
			return getObjectsWithSelectedValuesThatMatchSearchTerm(
					userArray, 
					getRelevantValuesFromUser, 
					searchTerm);
		}
	}]);
	
	
	
	customFilterModule.filter('userHasPermissionToSeeThisUser', ['LoginService', function(LoginService) {
		return function(userArray) {

			if(LoginService.loggedInAsAdmin()) {
				return userArray;
			} else {
				var filteredArray = [];
				var loggedInUser = LoginService.getUserObjectIfExists();
				
				for(var i = 0; i < userArray.length; i++) {
					var user = userArray[i];
					if(user.id === loggedInUser.id) {
						filteredArray.push(user);
					}
				}

				return filteredArray;
			}
		}
	}]);
	
	customFilterModule.filter('userHasPermissionToSeeThisCar', ['LoginService', function(LoginService) {
		return function(carArray) {

			if(LoginService.loggedInAsAdmin()) {
				return carArray;
			} else {
				var filteredArray = [];
				var loggedInUser = LoginService.getUserObjectIfExists();
				
				for(var i = 0; i < carArray.length; i++) {
					var car = carArray[i];
					if(car.owner.id === loggedInUser.id) {
						filteredArray.push(car);
					}
				}

				return filteredArray;
			}
		}
	}]);
	
	customFilterModule.filter('userHasPermissionToSeeThisAppointment', ['LoginService', function(LoginService) {
		return function(appointmentsArray) {

			if(LoginService.loggedInAsAdmin()) {
				return appointmentsArray;
			} else {
				var filteredArray = [];
				var loggedInUser = LoginService.getUserObjectIfExists();
				
				for(var i = 0; i < appointmentsArray.length; i++) {
					var appointment = appointmentsArray[i];
					if(appointment.carOwner.id === loggedInUser.id) {
						filteredArray.push(appointment);
					}
				}

				return filteredArray;
			}
		}
	}]);
	
	
	
	
	function getObjectsWithSelectedValuesThatMatchSearchTerm(objectArray, functionToExtractRelevantValuesFromSingleObject, searchTerm ) {
		
		if(!searchTerm) return objectArray;
		
		var matches = [];
		
		for(var i = 0; i < objectArray.length; i++) {
			
			var object = objectArray[i];
			
			var relevantValues = functionToExtractRelevantValuesFromSingleObject(object);
			
			var objectWasMatch = matchFoundInArray(relevantValues, searchTerm);
			
			if(objectWasMatch) {
				matches.push(object);
			}
		}
		
		return matches;
		
	}
	
	function carMatchesSearchTerm(car, searchTerm){
		
		var relevantValuesToCheck = getRelevantValuesFromCar(car);

		return matchFoundInArray(relevantValuesToCheck, searchTerm);
		
	}
	
	function getRelevantValuesFromUser(user) {
		return [
		        user.userContactInfo.firstName + " " + user.userContactInfo.lastName,
		        user.userType,
		        user.userContactInfo.email,
		        "" + user.userContactInfo.phoneNumber
		        ];
	}
	
	
	function getRelevantValuesFromAppointment(appointment) {
		return [
		        appointment.carOwner.userContactInfo.firstName + " " + appointment.carOwner.userContactInfo.lastName,
		        appointment.car.brand + " " + appointment.car.model,
		        appointment.problemDescription
		        ];
	}
	
	function getRelevantValuesFromCar(car) {
		return [
		        car.brand + " " + car.model,
		        "" + car.productionYear, 
		        car.propulsionTechnology,
		        car.owner.userContactInfo.firstName + " " + car.owner.userContactInfo.lastName,        
		        ];
	}
	
	function matchFoundInArray(valuesToCheck, searchTerm) {
		for(var i = 0; i < valuesToCheck.length; i++) {
			
			var value = valuesToCheck[i].toLowerCase();

			if(value.indexOf(searchTerm.toLowerCase()) != -1) {
				return true;
			}
		}
		
		return false;
	}
	
	
}());