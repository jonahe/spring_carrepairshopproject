Jämfört med tidigare projektet behövde följande göras:

Ändra pom.xml

Innom 

<project>

  <build>
       <plugins>
           <plugin>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-maven-plugin</artifactId>
               <executions>
                   <execution>
                       <goals>
                           <goal>repackage</goal>
                       </goals>
                   </execution>
               </executions>
           </plugin>
       </plugins>
   </build>
  
  <!-- added to make deployable .war -->
  <packaging>war</packaging>
  
  <dependencies>
    	<!-- added to make deployable .war -->
  	<dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-tomcat</artifactId>
            <scope>provided</scope>
    </dependency>

  </dependencies>

</project>
 

Ändra main-klassen till att extenda SpringBootServletInitializer
och overridea metoden configure:
	
	private static Class<Application> appClass = Application.class; // DIN klass som har main-metoden.

	private static Class<Application> appClass = Application.class;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	/** Added to make project deployable to Tomcat7 **/
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		
		return builder.sources(appClass);
	}


OCH ändra projektets setting. configure -> java compiler  -> välj 1.7 


kör Maven i kommandotolk: mvn package    ->  genererar en war-fil i  target/ mappen. 
döp om filen så att namnet stämmer överens med context-path. 
annars blir addressen  dindomän.se:8080/dittkonstigafilnamn/


läs mer på 
http://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#build-tool-plugins-maven-packaging
http://stackoverflow.com/questions/24741816/deploy-spring-boot-to-tomcat


http://erikwiberg.se:8080/repairshop-test/#/

Login:
	Admin users:
		admin@admin.se
		
	
	Customer users:
		johan@customer.se
		stina@customer.se
		emma@customer.se
		peter@customer.se
		najib@customer.se
		
		
